<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Photo;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * @param int $image_number
 * @return string
 */

if(!function_exists('image_path')) {
    function image_path(int $image_number): string
    {
        $path = storage_path() . "/images/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->encode('jpg');
        Storage::disk('public')->put('images/' . $image_name, $resize->__toString());
        return 'images/' . $image_name;
    }
}

$factory->define(Photo::class, function (Faker $faker) {
    return [
        'caption' => $faker->text($maxNbChars = 15),
        'user_id' => rand(1, 20),
        'image' => image_path(rand(1, 10)),
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->text($maxNbChars = 300),
        'user_id' => rand(1, 20),
        'photo_id' => rand(1, 50),
        'rate' => rand(1, 5)
    ];
});

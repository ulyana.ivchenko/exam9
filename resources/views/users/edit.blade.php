@extends('layouts.app')
@section('content')
    <div class="container mt-2">
        <div class="col-10">
            <h5>@lang('Edit user data'):</h5>
            <form method="post"
                  action="{{route('users.update', ['user' => $user])}}">
                @csrf
                @method('put')
                <div class="form-row mt-5">
                    <div class="form-group col-md-8">
                        <label for="name"><b>@lang('Name')</b></label><span class="text-danger">*</span>
                        <textarea rows="5" class="form-control @error('name') is-invalid @enderror" id="name"
                                  name="name">{{$user->name}}</textarea>
                        @error('name')
                        <p class="error">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary active pl-4 pr-4 mr-3">@lang('Edit')</button>
                <a href="{{route('users.show', ['user' => $user])}}">@lang('Back')</a>
            </form>
        </div>
    </div>
@endsection

@extends('layouts.app')
@section('content')
    <div class="container">
        @if (session('message'))
            <div class="alert alert-primary" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <div class="row">
            <h4><b>{{$user->name}}</b>@lang('\'s Profile')</h4>
            @if($user == Auth::user())
                <a class="ml-5" href="{{route('users.edit', ['user' => $user])}}">@lang('Edit')</a>
            @endif
        </div>
        <div class="row mt-3">
            <h5>@lang('My photos')</h5>
            @if($user == Auth::user())
                <a class="ml-5" href="{{route('photos.create')}}">@lang('Create new photo')</a>
            @endif
        </div>
        <div class="row mt-3">
            <table class="table table-bordered offset-md-2" style="width:60%">
                @foreach($user->photos as $photo)
                    <thead class="thead-light">
                    <tr class="text-center">
                        <th scope="col" style="width:10%">@lang('Photo')</th>
                        <th scope="col" style="width:40%">@lang('Title')</th>
                        @can('delete', $photo)
                            <th scope="col" style="width:1%">@lang('Actions')</th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <img class="img-thumbnail"
                                 src="{{asset('/storage/' . $photo->image)}}" alt="Photo"
                                 style="width: 100px; height: 75px">
                        </td>
                        <td>{{$photo->caption}}</td>
                        @can('delete', $photo)
                            <td>

                                <form method="post" action="{{route('photos.destroy', ['photo' => $photo])}}">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger">@lang('Удалить')
                                    </button>
                                </form>

                            </td>
                        @endcan
                    </tr>
                    @endforeach
                    </tbody>
            </table>
        </div>
    </div>
@endsection


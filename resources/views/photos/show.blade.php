@extends('layouts.app')
@section('content')
    <div class="container">
        @if (session('message'))
            <div class="alert alert-primary" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <h3 class="mb-2 mt-3">{{$photo->caption}}</h3>
                <img class="img-fluid" src="{{asset('/storage/' . $photo->image)}}" alt="Photo">
                <h5 class="mt-2">@lang('Average score'): {{$average_score}}</h5>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 mt-5">
                <h3>@lang('Add comment')</h3>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-7">
                @csrf
                @foreach($photo->comments as $comment)
                    <div class="media" id="delete-comment-{{$comment->id}}">
                        <img class="d-flex rounded-circle mt-0 mr-3" style="width: 50px; height: 50px"
                             src="{{asset('/images/default_photo.jpeg')}}" alt="Image">
                        <div class="media-body">
                            <div class="mb-3">
                                @can('delete', $comment)
                                    <form method="post"
                                          action="{{route('photos.comments.destroy', ['photo' => $photo, 'comment' => $comment])}}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="delete btn btn-light" aria-hidden="true"
                                                style="float: right"
                                        >&times;
                                        </button>
                                    </form>
                                @endcan
                            </div>
                            <div class="comment">
                                <p>@lang('By'): <b>{{$comment->user->name}}</b>,
                                    <span> @lang('score'): {{$comment->rate}}</span></p>
                            </div>
                            <p class="pr-4">{{$comment->body}}</p>
                            @can('update', $comment)
                                <a href="{{route('photos.comments.edit', ['photo' => $photo, 'comment' => $comment])}}"
                                   style="font-size: 12px">@lang('Edit')</a>
                            @endcan
                            <p class="created-at">@lang('Created') {{$comment->created_at->diffForHumans()}}</p>
                            <hr>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-4 ml-auto mt-3">
                <div class="comment-form">
                    <form method="post" action="{{route('photos.comments.store', ['photo' => $photo])}}">
                        @csrf
                        <div class="form-group">
                            <label for="body">@lang('Comment')</label>
                            <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="body"
                                      id="body" rows="3"/></textarea>
                            @error('body')
                            <p class="error">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="rate">@lang('Score')</label>
                            <select name="rate" id="rate" class="form-control @error('rate') is-invalid @enderror"
                                    id="rate" required>
                                <option disabled selected>@lang('Choose score'):</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                            @error('rate')
                            <p class="error">{{$message}}</p>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">@lang('Add comment')
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row p-5">
            <div class="col-10 offset-5">
                {{$photo->comments->links()}}
            </div>
        </div>
    </div>
@endsection

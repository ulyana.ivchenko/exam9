@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col-6 mb-4 mt-5 offset-md-3">
            <h5>@lang('Add new photo'):</h5>
        </div>
        <div class="col-6 offset-md-3">
            <form method="post" action="{{route('photos.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="form-group row">
                        <label for="caption" class="col-sm-2 col-form-label">@lang('Caption')</label>
                        <div class="col-sm-10">
                            <input name="caption" class="form-control @error('caption') is-invalid @enderror"
                                   id="caption"/>
                            @error('caption')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="image" class="col-sm-2 col-form-label">@lang('Image')</label>
                        <div class="col-sm-10">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input  @error('image') is-invalid @enderror"
                                       id="customFile" name="image">
                                <label class="custom-file-label" for="customFile">@lang('Upload photo')</label>
                            </div>
                            @error('image')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                <button type="submit" class="btn btn-primary active pl-4 pr-4 mr-3">@lang('Create')</button>
                <a href="{{route('photos.index')}}">@lang('Back')</a>
            </form>
        </div>
    </div>
@endsection

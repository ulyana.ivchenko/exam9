@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row d-flex justify-content-center">
            @foreach($photos as $photo)
                <div class="col-md-3 mt-4">
                    <div class="card ml-0 mr-0">
                        <img class="img-thumbnail"
                             src="{{asset('/storage/' . $photo->image)}}" alt="Photo"
                             style="width: 300px; height: 200px">
                        <div class="card-footer">
                            <div>
                                <p class="caption m-0" style="font-size: 15px;">
                                    <a href="{{route('photos.show', ['photo' =>$photo])}}">{{ $photo->caption }}</a>
                                </p>
                                <p class="mb-0">@lang('By'):
                                    <a href="{{route('users.show', ['user' =>$photo->user])}}"> {{$photo->user->name}}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row p-5">
            <div class="col-10 offset-4">
                {{ $photos->links() }}
            </div>
        </div>
    </div>

@endsection

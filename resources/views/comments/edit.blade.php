@extends('layouts.app')
@section('content')
    <div class="container mt-2">
        <div class="col-10">
            <h5>Edit comment:</h5>
            <form method="post"
                  action="{{route('photos.comments.update', ['comment' => $comment, 'photo' => $photo])}}">
                @csrf
                @method('put')
                <div class="form-row mt-5">
                    <div class="form-group col-md-8">
                        <label for="body"><b>Comment</b></label><span class="text-danger">*</span>
                        <textarea rows="5" class="form-control @error('body') is-invalid @enderror" id="body"
                                  name="body">{{$comment->body}}</textarea>
                        @error('body')
                        <p class="error">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-group col-md-8">
                        <label for="rate"><b>Score</b></label><span class="text-danger">*</span>
                        <select name="rate" id="rate" class="form-control @error('rate') is-invalid @enderror"
                                value="{{$comment->rate}}">
                            <option disabled>Choose score:</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                        @error('rate')
                        <p class="error">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary active pl-4 pr-4 mr-3">Edit</button>
                <a href="{{route('photos.show', ['photo' => $photo])}}">Back</a>
            </form>
        </div>
    </div>
@endsection

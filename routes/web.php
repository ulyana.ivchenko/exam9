<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'language'], function () {
    Route::get('/', 'PhotosController@index')->name('mainpage');
    Route::resource('photos', 'PhotosController')->except('edit', 'update');
    Route::resource('users', 'UsersController')->except('index', 'create', 'store', 'destroy');
    Route::resource('photos.comments', 'CommentsController')->except('index', 'create')->middleware('auth');
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::get('language/{locale}', 'LanguageSwitcherController@switcher')
    ->name('language.switcher')
    ->where('locale', 'en|ru');

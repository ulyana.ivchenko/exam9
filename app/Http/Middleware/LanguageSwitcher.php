<?php

namespace App\Http\Middleware;

use Closure;

class LanguageSwitcher
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->exists('locale')) {
            $request->session()->put('locale', config('app.locale', 'en'));
        }
        app()->setLocale($request->session()->get('locale'));
        return $next($request);
    }
}

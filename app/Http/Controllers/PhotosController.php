<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\PhotoRequest;
use App\Photo;
use App\User;
use Illuminate\Support\Facades\Auth;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $photos = Photo::select('id', 'user_id', 'caption', 'image', 'created_at', 'updated_at')
            ->latest()->paginate(8);
        return view('photos.index', compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('photos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(PhotoRequest $request)
    {
        $validated = $request->validated();
        $user = Auth::user();
        $photo = new Photo();

        $data = $request->all();
        $file = $request->file('image');
        if (!is_null($file)) {
            $path = $file->store('images', 'public');
            $data['image'] = $path;
        }

        $photo->user_id = $request->user()->id;
        $data['user_id'] = $photo->user_id;

        $photo->create($data);

        return redirect()->route('users.show', ['user' => $user])->with('message', 'New photo added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $photo = Photo::findOrFail($id);
        $photo->setRelation('comments', $photo->comments()->latest()->paginate(3));
        $comments = Comment::all();
        $total_comments = $comments->count();
        $total_score = 0;
        foreach($comments as $comment) {
            $total_score += $comment->rate;
        }
        $average_score = round(($total_score / $total_comments), 2);
        return view('photos.show', compact('photo', 'comments', 'average_score'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $photo = Photo::findOrFail($id);
        $this->authorize('delete', $photo);
        $photo->comments()->delete();
        $photo->delete();
        return redirect()->route('users.show', ['user' => $user])->with('message', 'Photo deleted!');
    }

}

<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    /**
     * @param CommentRequest $request
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function store(CommentRequest $request, Photo $photo)
    {
        $validated = $request->validated();

        Auth::user();

        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->rate = $request->input('rate');
        $comment->photo_id = $photo->id;
        $comment->user_id = $request->user()->id;
        $comment->save();

        return redirect()->route('photos.show', compact('photo'))->with('message', 'New comment added!');
    }


    /**
     * @param Photo $photo
     * @param Comment $comment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Photo $photo, Comment $comment)
    {
        $this->authorize('update', $comment);
        return view('comments.edit', compact('comment', 'photo'));
    }

    /**
     * @param CommentRequest $request
     * @param $photo
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CommentRequest $request, $photo, Comment $comment)
    {
        $comment->update($request->all());
        return redirect(route('photos.show', compact('comment', 'photo')))->with('message', 'Comment updated!');
    }


    /**
     * @param Photo $photo
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Photo $photo, $id)
    {
        $comment = Comment::findOrFail($id);
        $this->authorize('delete', $comment);
        $comment->delete();
        return redirect()->route('photos.show', compact('photo'))->with('message', 'Comment deleted!');
    }
}

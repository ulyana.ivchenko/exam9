<?php

namespace App\Orchid\Layouts;

use App\Photo;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PhotoListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'photos';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
                ->sort()
                ->render(function (Photo $photo) {
                    return Link::make($photo->id)
                        ->route('platform.photos.edit', $photo);
                })->align('center'),
            TD::set('caption', 'Title'),
            TD::set('image', 'Image')
                ->width('300px')
                ->align('center'),
            TD::set('created_at', 'Created')->width('150px')->sort(),
            TD::set('updated_at', 'Updated')->width('150px')->sort(),
        ];
    }
}

<?php

namespace App\Orchid\Layouts;

use App\Comment;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
                ->sort()
                ->render(function (Comment $comment) {
                    return Link::make($comment->id)
                        ->route('platform.comments.edit', $comment);
                })->align('center'),
            TD::set('body', 'Comment')
                ->width('490px'),
            TD::set('rate', 'Score')
                ->align('center'),
            TD::set('created_at', 'Created')->width('80px')->sort(),
            TD::set('updated_at', 'Updated')->width('80px')->sort(),
        ];
    }
}

<?php

namespace App\Orchid\Screens;

use App\Orchid\Layouts\PhotoListLayout;
use App\Photo;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class PhotoListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Photos';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'All photos';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'photos' => Photo::filters()->defaultSort('created_at')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new')
                ->icon('icon-pencil')
                ->route('platform.photos.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            PhotoListLayout::class
        ];
    }
}

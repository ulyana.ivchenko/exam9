<?php

namespace App\Orchid\Screens;

use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Orchid\Attachment\File;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class PhotoEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'PhotoEditScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Photo $photo): array
    {
        $this->exists = $photo->exists;
        if ($this->exists) {
            $this->name = 'Edit photo';
        }
        return [
            'photo' => $photo
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')
                ->method('update')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('delete')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('photo.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),
                Input::make('photo.caption')
                    ->title('Title')
                    ->required(),
                Upload::make('images')
                    ->groups('photo.image')
            ])
        ];
    }

    /**
     * @param Photo $photo
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Photo $photo, Request $request)
    {
        $file = new File($request->file('photo'));
        $photo = $file->load();
        $photo->fill($request->get('photo'))->save();
        Alert::info('You have updated the photo');
        return redirect()->route('platform.photos.list');
    }

    /**
     * @param Photo $photo
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Photo $photo, Request $request)
    {
        $file = new File($request->file('image'));
        $photo = $file->load();
        $photo->fill($request->get('photo'))->save();
        Alert::info('You have updated the photo');
        return redirect()->route('platform.photos.list');
    }

    /**
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Photo $photo)
    {
        $photo->delete()
            ? Alert::info('You have successfully deleted the photo')
            : Alert::warning('An error has occurred');
        return redirect()->route('platform.photos.list');
    }
}

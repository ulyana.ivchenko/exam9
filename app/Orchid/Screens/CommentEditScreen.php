<?php

namespace App\Orchid\Screens;

use App\Comment;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = '';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Comment $comment): array
    {
        $this->exists = $comment->exists;
        if($this->exists){
            $this->name = 'Edit comment';
        }
        return [
            'comment' => $comment
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')
                ->method('update')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('delete')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('comment.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),
                Relation::make('comment.photo_id')
                    ->title('Title')
                    ->fromModel(Photo::class, 'caption', 'id'),
                TextArea::make('comment.body')->title('Comment')->rows(5)->required(),
                Select::make('comment.rate')
                    ->title('Score')
                    ->options([1, 2, 3, 4, 5])
                    ->required()
            ])
        ];
    }

    /**
     * @param Comment $comment
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('You have created a new comment');
        return redirect()->route('platform.comments.list');
    }

    /**
     * @param Comment $comment
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('You have updated the comment');
        return redirect()->route('platform.comments.list');
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('You have successfully deleted the comment')
            : Alert::warning('An error has occurred')
        ;
        return redirect()->route('platform.comments.list');
    }
}

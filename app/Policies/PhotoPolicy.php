<?php

namespace App\Policies;

use App\Photo;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PhotoPolicy
{
    use HandlesAuthorization;


    /**
     * @param User $user
     * @param Photo $photo
     * @return bool
     */
    public function delete(User $user, Photo $photo)
    {
        return $user->id == $photo->user_id;
    }
}

<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    private $user;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_success_edit_own_data()
    {
        $this->actingAs($this->user);
        $data = [
            'name' => 'Test name'
        ];
        $response = $this->put(route('users.update', ['user' => $this->user], $data));
        $response->assertStatus(302);
        $this->assertDatabaseHas('users', $data);
    }
}

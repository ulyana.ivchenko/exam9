<?php

namespace Tests\Feature;

use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PhotoTest extends TestCase
{
    use DatabaseTransactions;

    private $user, $photos;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function test_success_see_registration()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function test_success_see_login()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function test_success_show_all_photos()
    {
        $response = $this->get(route('photos.index'));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function test_success_show_photo()
    {
        $photo = factory(Photo::class)->create();

        $response = $this->get(route('photos.show', ['photo' => $photo]));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function test_success_get_create_photo()
    {
        $this->actingAs($this->user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $response = $this->get(route('photos.create', ['photo' => $photo]));
        $response->assertStatus(200);
        $response->assertSeeText('Add new photo');
        $response->assertSeeText('Caption');
        $response->assertSeeText('Image');
        $response->assertSeeText('Create');
        $response->assertSeeText('Back');
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function test_success_delete_own_photo()
    {
        $this->actingAs($this->user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $response = $this->delete(route('photos.destroy', ['photo' => $photo]));
        $response->assertStatus(302);
        $response->assertRedirect(route('users.show', ['user' => $this->user]));
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function test_failed_delete_not_own_photo()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $response = $this->delete(route('photos.destroy', ['photo' => $photo]));
        $response->assertStatus(403);
    }
}

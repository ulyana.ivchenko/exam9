<?php

namespace Tests\Feature;

use App\Comment;
use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;e;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use DatabaseTransactions;

    private $user, $comments;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_success_see_comments()
    {
        $this->actingAs($this->user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'photo_id' => $photo->id,
        ]);

        $response = $this->get('/photos/' . $photo->id);
        $response->assertStatus(200);

        $response->assertSeeText($comment->body);
        $response->assertSeeText($comment->user->name);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_failed_create_comment_if_not_auth()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $data = [
            'body' => 'Test body data',
            'rate' => 4
        ];

        $response = $this->post(route('photos.comments.store', ['photo' => $photo]), $data);
        $response->assertStatus(302);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_success_create_comment()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($this->user);

        $photo = factory(Photo::class)->create([
            'user_id' => $another_user->id,
        ]);

        $data = [
            'body' => 'Test body data',
            'rate' => 4
        ];

        $response = $this->post(route('photos.comments.store', ['photo' => $photo]), $data);
        $response->assertRedirect(route('photos.show', ['photo' => $photo]));
        $this->assertDatabaseHas('comments', $data);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_success_get_edit_own_comment()
    {
        $this->actingAs($this->user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'photo_id' => $photo->id,
        ]);

        $response = $this->get(route('photos.comments.edit', ['photo' => $photo, 'comment' => $comment]));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_failed_get_edit_not_own_comment()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'photo_id' => $photo->id,
        ]);

        $response = $this->get(route('photos.comments.edit', ['photo' => $photo, 'comment' => $comment]));
        $response->assertStatus(403);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_success_edit_own_comment()
    {
        $this->actingAs($this->user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'photo_id' => $photo->id,
        ]);

        $data = [
            'body' => 'Test body data',
            'rate' => 4
        ];

        $response = $this->put(route('photos.comments.update', ['photo' => $photo, 'comment' => $comment]), $data);
        $response->assertRedirect(route('photos.show', ['photo' => $photo, 'comment' => $comment]));
        $this->assertDatabaseHas('comments', $data);
    }


    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_failed_delete_not_own_comment()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'photo_id' => $photo->id,
        ]);

        $response = $this->delete(route('photos.comments.destroy', ['photo' => $photo, 'comment' => $comment]));
        $response->assertStatus(403);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function test_success_delete_own_comment()
    {
        $this->actingAs($this->user);

        $photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'photo_id' => $photo->id,
        ]);

        $response = $this->delete(route('photos.comments.destroy', ['photo' => $photo, 'comment' => $comment]));
        $response->assertRedirect(route('photos.show', ['photo' => $photo]));
    }
}
